package lab.studies.web;

import lab.studies.model.Student;
import lab.studies.service.StudiesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class StudiesRest {

    private static final Logger log = Logger.getLogger(StudiesRest.class.getName());

    private final StudiesService service;

    public StudiesRest(StudiesService service) {
        this.service = service;
    }

    @GetMapping("/students")
    List<Student> getStudents(){
        log.info("retrieving student list");
        return service.getStudents();
    }

    @PostMapping("/students")
    Student addStudent(@RequestBody Student student){
        log.info("registering student " + student);
        return service.registerStudent(student);
    }
}

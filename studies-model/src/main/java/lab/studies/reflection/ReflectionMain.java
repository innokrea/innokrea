package lab.studies.reflection;

import lab.studies.model.Positive;
import lab.studies.model.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class ReflectionMain {

    public static void main(String[] args) throws ClassNotFoundException {
        System.out.println("Let's reflect!");

        Object o = getObject();
        hijackObject(o, "id", 11);

        validate(o);

        Class<?> clazz = o.getClass();
                // Class.forName("lab.studies.model.Student");
                // Student.class;
        do {
            ClassViewer viewer = new ClassViewer(clazz);
            viewer.printClassInfo();
            viewer.printFieldsInfo();
            viewer.printConstructors();
            viewer.printMethodInfo();
            clazz = clazz.getSuperclass();
        }while(clazz!=Object.class);

    }

    static void hijackObject(Object o, String fieldName, Object fieldValue){
        Field field = getField(o.getClass(), fieldName);
        try {
            field.setAccessible(true);
            System.out.println("hijacking field " + fieldName + ", current value " + field.get(o));
            field.set(o, fieldValue);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    static void validate(Object o){

        Class clazz = o.getClass();
        do {
            Arrays.stream(clazz.getDeclaredFields())
                    .forEach(f -> {
                        try {
                            if (f.isAnnotationPresent(Positive.class)) {
                                Positive positive = f.getAnnotation(Positive.class);
                                f.setAccessible(true);
                                Object value = f.get(o);
                                if (value != null && Number.class.isAssignableFrom(value.getClass())) {
                                    int intValue = ((Number) value).intValue();
                                    if (intValue < 0 || intValue> positive.max()) {
                                        throw new IllegalArgumentException("field " + f.getName() + " should be positive but the value is " + value);
                                    }
                                }
                            }
                        } catch (IllegalAccessException e) {
                            throw new RuntimeException(e);
                        }
                    });
            clazz = clazz.getSuperclass();
        }while(clazz!= Object.class);
    }

    static Object getObject(){
        String className = "lab.studies.model.Teacher";
        int id = 1;
        String firstName = "Jan";
        String lastName = "Kowalski";

        try {
            Class clazz = Class.forName(className);
            Constructor constructor = clazz.getConstructor(int.class, String.class, String.class);
            Object o = constructor.newInstance(id, firstName, lastName);
            return o;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        //return new Student(1, "Jan", "kowalski");
    }

    private static Field getField(Class clazz, String fieldName){

        Field field = null;
        while(field==null && clazz!=null) {
            try {
                field = clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }
        return field;
    }
}

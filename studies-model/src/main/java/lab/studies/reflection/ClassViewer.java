package lab.studies.reflection;

import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ClassViewer {

    private Class clazz;

    public ClassViewer(Class clazz) {
        this.clazz = clazz;
    }

    void printClassInfo() {
        System.out.println("--------------------------------------");
        System.out.println("Class name: " + clazz.getSimpleName()); // sout
        System.out.println("Package name: " + clazz.getPackageName());
        System.out.println("Modifiers: " + modifiersString(clazz.getModifiers()));
    }


    void printFieldsInfo() {
        Arrays.stream(clazz.getDeclaredFields())
                .forEach(f -> {
                    System.out.println("field name: " + f.getName());
                    System.out.println("field type: " + f.getType().getName());
                    System.out.println("field modifiers: " + modifiersString(f.getModifiers()));
                        }
                );
    }

    void printConstructors(){
        Arrays.stream(clazz.getConstructors())
                .forEach( c->{
                    System.out.println("constructor params: " + classesString(c.getParameterTypes()));
                    System.out.println("constructor modifiers: " + modifiersString(c.getModifiers()));
                    System.out.println("constructor exceptions: " + classesString(c.getExceptionTypes()));
                });
    }

    void printMethodInfo(){
        Arrays.stream(clazz.getDeclaredMethods())
                .forEach(m->{
                    System.out.println("method name: " + m.getName());
                    System.out.println("method return type: " + m.getReturnType().getName());
                    System.out.println("method parameters: " + classesString(m.getParameterTypes()));
                    System.out.println("method exceptions: " + classesString(m.getExceptionTypes()));
                });
    }


    private String modifiersString(int modifiers) {

        StringBuilder builder = new StringBuilder();
        builder.append("public: ").append(Modifier.isPublic(modifiers));
        builder.append(",protected: ").append(Modifier.isProtected(modifiers));
        builder.append(",private: ").append(Modifier.isPrivate(modifiers));
        builder.append(",abstract: ").append(Modifier.isAbstract(modifiers));
        builder.append(",final: ").append(Modifier.isFinal(modifiers));
        builder.append(",static: ").append(Modifier.isStatic(modifiers));

        return builder.toString();
    }

    private String classesString(Class[] classes){
        return Arrays.asList(classes).toString();
    }


}

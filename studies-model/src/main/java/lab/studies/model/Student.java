package lab.studies.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Student extends Person {

    @ManyToMany(mappedBy = "students", fetch = FetchType.EAGER)
    private List<Subject> subjects = new ArrayList<>();

    @OneToMany(mappedBy = "student")
    @JsonIgnore
    private List<Grade> grades = new ArrayList<>();

    public Student(int id, String firstName, String lastName) {
        super(id, firstName, lastName);
    }

    public Student() {
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
        subject.addStudent(this);
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "Student{" +
                " person=" + super.toString() +
                '}';
    }
}

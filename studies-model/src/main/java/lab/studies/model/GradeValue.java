package lab.studies.model;

public enum GradeValue {
    A,
    B,
    C,
    D,
    E,
    F
}

package lab.studies.dao.memory;

import lab.studies.dao.TeacherDao;
import lab.studies.model.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MemoryTeacherDao implements TeacherDao {
    @Override
    public Teacher save(Teacher teacher) {
        int maxId = MemoryData.teachers.stream().mapToInt(t->t.getId()).max().orElse(0);
        teacher.setId(maxId);
        MemoryData.teachers.add(teacher);
        return teacher;
    }

    @Override
    public List<Teacher> findAll() {
        return MemoryData.teachers;
    }

    @Override
    public Optional<Teacher> findById(int id) {
        return MemoryData.teachers.stream().filter(s->s.getId()==id).findFirst();
    }
}

package lab.studies.dao.memory;

import lab.studies.dao.SubjectDao;
import lab.studies.model.Subject;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MemorySubjectDao implements SubjectDao {

    @Override
    public Subject save(Subject subject) {
        int maxId = MemoryData.subjects.stream().mapToInt(t->t.getId()).max().orElse(0);
        subject.setId(maxId);
        MemoryData.subjects.add(subject);
        return subject;
    }

    @Override
    public List<Subject> findAll() {
        return MemoryData.subjects;
    }

    @Override
    public Optional<Subject> findById(int id) {
        return MemoryData.subjects.stream().filter(s->s.getId()==id).findFirst();
    }

    @Override
    public Optional<Subject> findByName(String name) {
        return MemoryData.subjects.stream().filter(s->s.getName()==name).findFirst();
    }
}

package lab.studies.dao.memory;

import lab.studies.model.Grade;
import lab.studies.model.Student;
import lab.studies.model.Subject;
import lab.studies.model.Teacher;

import java.util.ArrayList;
import java.util.List;

public class MemoryData {

    static final List<Teacher> teachers = new ArrayList<>();

    static final List<Student> students = new ArrayList<>();

    static final List<Subject> subjects = new ArrayList<>();

    static final List<Grade> grades = new ArrayList<>();
}

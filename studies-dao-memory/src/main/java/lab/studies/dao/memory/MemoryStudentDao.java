package lab.studies.dao.memory;

import lab.studies.dao.StudentDao;
import lab.studies.model.Student;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MemoryStudentDao implements StudentDao {
    @Override
    public Student save(Student student) {
        int maxId = MemoryData.students.stream().mapToInt(t->t.getId()).max().orElse(0);
        student.setId(maxId);
        MemoryData.students.add(student);
        return student;
    }

    @Override
    public List<Student> findAll() {
        return MemoryData.students;
    }

    @Override
    public Optional<Student> findById(int id) {
        return MemoryData.students.stream().filter(s->s.getId()==id).findFirst();
    }
}

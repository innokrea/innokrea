package lab.studies.service;

import lab.studies.dao.GradeDao;
import lab.studies.dao.StudentDao;
import lab.studies.dao.SubjectDao;
import lab.studies.dao.TeacherDao;
import lab.studies.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

@Service
public class StudiesService {

    private static final Logger log = Logger.getLogger(StudiesService.class.getName());

    private final TeacherDao teacherDao;
    private final StudentDao studentDao;
    private final SubjectDao subjectDao;
    private final GradeDao gradeDao;

    public StudiesService(TeacherDao teacherDao, StudentDao studentDao, SubjectDao subjectDao, GradeDao gradeDao) {
        this.teacherDao = teacherDao;
        this.studentDao = studentDao;
        this.subjectDao = subjectDao;
        this.gradeDao = gradeDao;
    }

    public Teacher hireTeacher(Teacher teacher){
        log.info("hiring teacher " + teacher);
        return teacherDao.save(teacher);
    }

    @Transactional
    public Student registerStudent(Student student){
        log.info("registering student " + student);
        return studentDao.save(student);
    }

    public Subject defineSubject(Teacher teacher, String name){
        Subject subject = new Subject();
        subject.setTeacher(teacher);
        subject.setName(name);
        return subjectDao.save(subject);
    }

    public void assignToSubject(Student student, Subject subject){
        student.getSubjects().add(subject);
        subject.addStudent(student);
    }

    public void addGrade(Student student, Subject subject, GradeValue gradeValue){
        Grade grade = new Grade(gradeValue, student, subject);
        gradeDao.save(grade);
    }

    public List<Student> getStudents(){
        return studentDao.findAll();
    }




}

package lab.studies.dao;

import lab.studies.model.Grade;

import java.util.List;
import java.util.Optional;

public interface GradeDao {

    Grade save(Grade grade);

    List<Grade> findAll();

    Optional<Grade> findById(int id);
}

package lab.studies.dao;

import lab.studies.model.Student;

import java.util.List;
import java.util.Optional;

public interface StudentDao {

    Student save(Student student);

    List<Student> findAll();

    Optional<Student> findById(int id);

}

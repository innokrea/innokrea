package lab.studies.dao.jpa;

import lab.studies.dao.StudentDao;
import lab.studies.model.Student;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Repository
@Primary
public class JpaStudentDao implements StudentDao {

    @PersistenceContext(unitName = "studies")
    private EntityManager entityManager;

    @Override
    public Student save(Student student) {
        entityManager.persist(student);
        return student;
    }

    @Override
    public List<Student> findAll() {
        // JPQL -> HQL -> SQL
        return entityManager.createQuery("select s from Student s").getResultList();
    }

    @Override
    public Optional<Student> findById(int id) {
        return Optional.ofNullable(entityManager.find(Student.class, id));
    }
}
